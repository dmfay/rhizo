'use strict';

const {assert} = require('chai');
const rhizo = require('../index.js');

const fixtures = {
  one: async () => {
    return new Promise(resolve => {
      setTimeout(() => {
        return resolve('one');
      }, 10);
    });
  },
  two: (environment, state) => {
    return Promise.resolve(state.one + 'two');
  },
  three: (environment) => {
    return Promise.resolve(environment.envField);
  }
};

describe('rhizo', function () {
  it('returns a function', async function () {
    const stateFactory = await rhizo(fixtures, {});

    assert.isFunction(stateFactory);
  });

  it('executes defined fixtures', async function () {
    const stateFactory = await rhizo(fixtures, {});

    const finalState = await stateFactory({}, 'one', 'two');

    assert.isOk(finalState);
    assert.equal(finalState.one, 'one');
    assert.equal(finalState.two, 'onetwo');
    assert.isUndefined(finalState.three);
  });

  it('executes inline function fixtures', async function () {
    const stateFactory = await rhizo(fixtures, {});

    const finalState = await stateFactory(
      {},
      'one',
      async (env, state) => {
        state.func = 'hi!';

        return state;
      },
      'two'
    );

    assert.isOk(finalState);
    assert.equal(finalState.one, 'one');
    assert.equal(finalState.two, 'onetwo');
    assert.equal(finalState.func, 'hi!');
  });

  it('supplies an environment to fixtures', async function () {
    const stateFactory = await rhizo(fixtures, {});

    const finalState = await stateFactory({envField: 'envValue'}, 'one', 'two', 'three');

    assert.isOk(finalState);
    assert.equal(finalState.one, 'one');
    assert.equal(finalState.two, 'onetwo');
    assert.equal(finalState.three, 'envValue');
  });

  it('composes environments', async function () {
    const stateFactory = await rhizo(fixtures, {});

    const initialState = await stateFactory({}, 'one', 'two');

    assert.isOk(initialState);
    assert.equal(initialState.one, 'one');
    assert.equal(initialState.two, 'onetwo');
    assert.notOk(initialState.three);

    const finalState = await stateFactory({
      envField: 'envValue',
      state: initialState
    }, 'three');

    assert.isOk(finalState);
    assert.equal(finalState.one, 'one');
    assert.equal(finalState.two, 'onetwo');
    assert.equal(finalState.three, 'envValue');
  });
});
