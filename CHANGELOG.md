# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.2.0](https://gitlab.com/dmfay/rhizo/compare/v1.1.0...v1.2.0) (2019-05-18)


### Features

* inline function fixtures ([43be5ba](https://gitlab.com/dmfay/rhizo/commit/43be5ba))
