'use strict';

/**
 * Rhizo creates a stateFactory function which, when invoked, runs fixture
 * methods in sequence to build a state object.
 *
 * @param {Module|Object} fixtures - A module or object containing named fixture
 * functions with the signature (environment, state). The value returned by the
 * fixture is stored in the state under the fixture's name.
 * @return {Function} A stateFactory which executes fixtures against the base
 * state.
 */
exports = module.exports = (fixtures) => {
  /**
   * The stateFactory builds a representation of an external data-state by
   * running a selection of the available fixtures in order.
   *
   * @param {Object} environment - Contains any properties, object instances,
   * etc fixture functions need (such as a database connection).
   * @param {...String|Function} active - The names of specific fixture functions to run.
   * @return {Object} The completed data-state.
   */
  return function stateFactory (environment, ...active) {
    return active.reduce((promise, current) => {
      // If the current fixture is a function, just run it. It is expected to
      // modify and return the state itself.
      if (typeof current === 'function') {
        return promise.then(state => {
          return current(environment, state);
        });
      }

      // Otherwise, if the current fixture is a name, look it up in the
      // fixtures collection passed on init. The return value is added to the
      // state dictionary under the fixture's name.
      return promise.then(state => {
        return fixtures[current](environment, state)
          .then(result => {
            state[current] = result;

            return state;
          });
      });
    }, Promise.resolve(environment.state || {}));
  };
};
